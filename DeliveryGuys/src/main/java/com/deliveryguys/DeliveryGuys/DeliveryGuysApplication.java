package com.deliveryguys.DeliveryGuys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliveryGuysApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliveryGuysApplication.class, args);
	}

}
