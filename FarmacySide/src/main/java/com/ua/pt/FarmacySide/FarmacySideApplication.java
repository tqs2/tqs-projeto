package com.ua.pt.FarmacySide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarmacySideApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarmacySideApplication.class, args);
	}

}
